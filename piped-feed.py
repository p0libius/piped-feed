import feedparser
import re
import subprocess
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("feed", type=str,
                    help="fetches given piped subscriptions feed and allows to download or stream chosen video")
parser.add_argument("-a", "--arg", help="passes argument to downloader / player")
parser.add_argument("-l", "--link", help="returns original video link to console", action="store_true")
parser.add_argument("-q", "--quality", help="passes given video / audio quality to downloader / player")
parser.add_argument("-s", "--stream", help="stream video through mpv", action="store_true")
args = parser.parse_args()

pipedFeed = feedparser.parse(args.feed)

print('Number of RSS posts :', len(pipedFeed.entries))
for i in range(len(pipedFeed.entries)):
  print(i, pipedFeed.entries[i].title, "-", pipedFeed.entries[i].author)
selectedVideoNo = int(input('Video number: '))
originLink=re.sub("^.*(?=(\/watch))", "https://www.youtube.com",pipedFeed.entries[selectedVideoNo].link)
originLink=re.sub("^.*(?=(\/channel))", "https://www.youtube.com",originLink)
originLink=re.sub("^.*(?=(\/playlist))", "https://www.youtube.com",originLink)
originLink=re.sub("^.*(?=(\/@))", "https://www.youtube.com",originLink)
subargs = originLink
if args.link:
  print(originLink)
if args.arg:
  subargs = subargs + ' ' + args.arg
if args.quality:
  if args.stream:
    subargs = subargs + ' --ytdl-format=' + args.quality
  else:
    subargs = subargs + ' -f ' + args.quality
if args.stream:
  print('mpv ' + subargs)
  subprocess.Popen('mpv ' + subargs)
else:
  print('yt-dlp ' + subargs)
  subprocess.Popen('yt-dlp ' + subargs)