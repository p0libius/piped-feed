# piped-feed

Fetches given piped subscriptions feed and allows to download or stream chosen video.

Requires yt-dlp and mpv.